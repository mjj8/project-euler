
fib :: (Integral a) => a -> a
fib 0 = 1
fib 1 = 1
fib n = fib(n - 1) + fib(n - 2)

isEven :: (Integral a) => a -> Bool
isEven n = n `mod` 2 == 0

main = do
    let evenfiblist = filter isEven $ map fib [1..]
    let belowLimit = takeWhile (<4000000) evenfiblist
    let res = sum belowLimit
    print(res)
