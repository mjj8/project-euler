#!/usr/bin/env stack
-- stack script --resolver lts-20.8

{-
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
-}

multof3or5 :: Int -> Bool
multof3or5 x = (mod x 3 == 0) || (mod x 5 == 0)

prob1 :: Int -> Int
prob1 n = sum $ filter multof3or5 [1..n-1]

main :: IO ()
main = print $ prob1 1000
