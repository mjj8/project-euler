#!/usr/bin/env python3

# Find the sum of even-valued terms in the Fibonacci sequence whose values do
# not exceed four million


def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)

res = 0
value = 0
n = 0
while value < 4e6:
    value = fib(n)
    n += 1
    if value % 2 == 0:
        res += value

print(res)
